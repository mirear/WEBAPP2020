package ru.mirea.webapp.repository;

import org.springframework.data.repository.CrudRepository;
import ru.mirea.webapp.domain.Cat;

public interface CatDAO extends CrudRepository<Cat, Integer> {
}
