package ru.mirea.webapp;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Double checking singleton pattern
 */
public class TraditionalSingleton {

    private static volatile TraditionalSingleton instance;

    private static Lock lock = new ReentrantLock();

    private TraditionalSingleton() {
    }

    public static TraditionalSingleton getInstance() {
        if (instance == null) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new TraditionalSingleton();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }
}
