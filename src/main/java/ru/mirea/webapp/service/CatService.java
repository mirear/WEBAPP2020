package ru.mirea.webapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mirea.webapp.domain.Cat;
import ru.mirea.webapp.repository.CatDAO;

import javax.transaction.Transactional;

@Service
@Transactional
public class CatService {

    private CatDAO catDAO;

    @Autowired
    public CatService(CatDAO catDAO) {
        this.catDAO = catDAO;
    }

    public Iterable<Cat> findAll() {
        return getAll();
    }

    public Cat findById(Integer id) {
        return getById(id);
    }

    private Iterable<Cat> getAll() {
        return catDAO.findAll();
    }

    private Cat getById(Integer id) {
        return catDAO.findById(id).get();
    }

    public Cat add(Cat cat) {
        return addCat(cat);
    }

    private Cat addCat(Cat cat) {
        return catDAO.save(cat);
    }
}
