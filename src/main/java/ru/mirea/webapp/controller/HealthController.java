package ru.mirea.webapp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8080/health_check -> ok
 */

@RestController
public class HealthController {

    @GetMapping("health_check")
    public String healthCheck() {
        return "ok\n";
    }

}
