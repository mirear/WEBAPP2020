package ru.mirea.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.webapp.domain.Cat;
import ru.mirea.webapp.service.CatService;

/**
 * create controller with urls:
 * 1) /cat - return cats list (json format)
 * 2) /cat/{id} - return cat by cats id (json format)
 */

@RestController
public class CatController {

    private CatService catService;

    @Autowired
    public CatController(CatService catService) {
        this.catService = catService;
    }

    @GetMapping("/cat")
    public Iterable<Cat> cats() {
        System.out.println("Requesting cats list");
        return catService.findAll();
    }

    @PutMapping("/cat")
    public Cat addCat(@RequestBody Cat cat) {
        return catService.add(cat);
    }

    @GetMapping("/cat/{id}")
    public Cat cat(@PathVariable Integer id){
        System.out.println("Requesting cat with id: " + id);
        return catService.findById(id);
    }

}
